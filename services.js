import {
  CROSS_PLAYER,
  ROUND_PLAYER,
  MIN_NEEDED_TURNS,
  state,
  DEFAULT_CURRENT_TURN,
  DEFAULT_BOARD,
} from "./state";
import transpose from "./utils/transpose";

function increaseRoundScore() {
  state.round_score += 1;
}
function increaseCrossScore() {
  state.cross_score += 1;
}
function increaseDrawScore() {
  state.draw_score += 1;
}

function increaseGameNumber() {
  state.game_number += 1;
}

function increaseTurnsCounter() {
  state.turns_counter += 1;
}

function setWinner(value) {
  state.winner = value;
}

function setWinnerSeq(array) {
  state.winner_seq = array;
}

function switchPlayer() {
  state.current_turn === CROSS_PLAYER
    ? (state.current_turn = ROUND_PLAYER)
    : (state.current_turn = CROSS_PLAYER);
}

function checkDraw() {
  return state.board.every((row) => row.every((item) => item !== null));
}

/**
 * @param {array} board - The game board.
 * @param {string} player - The player, who makes a turn. Allowed CROSS_PLAYER and ROUND_PLAYER
 * * */
function detectWinner(board, player) {
  // check major diagonal
  const majorDiagonal = board.map((value, index) => board[index][index]);
  if (majorDiagonal.every((item) => item === player)) {
    return board.map((value, index) => [index, index]);
  }

  // check minor diagonal
  const minorDiagonal = board.map(
    (value, index) => board[index][board.length - index - 1]
  );
  if (minorDiagonal.every((item) => item === player)) {
    return board.map((value, index) => [index, board.length - index - 1]);
  }

  // check rows
  const rowIndex = board.findIndex((row) =>
    row.every((item) => item === player)
  );
  if (rowIndex > -1) {
    return board.map((value, columnIndex) => [rowIndex, columnIndex]);
  }

  // check columns
  const transposeBoard = transpose(board);
  const columnIndex = transposeBoard.findIndex((row) =>
    row.every((item) => item === player)
  );
  if (columnIndex > -1) {
    return board.map((value, tRowIndex) => [tRowIndex, columnIndex]);
  }
  return null;
}

function chooseWinner() {
  state.current_turn === CROSS_PLAYER
    ? increaseCrossScore()
    : increaseRoundScore();
  setWinner(state.current_turn);
}

function checkWinner() {
  if (![CROSS_PLAYER, ROUND_PLAYER].includes(state.current_turn)) return false;
  if (state.turns_counter <= MIN_NEEDED_TURNS) return false;
  return detectWinner(state.board, state.current_turn);
}

/**
 * @param {number} row - Row index.
 * @param {number} column - Column index.
 * * */
function updateBoard(row, column) {
  state.board[row][column] = state.current_turn;
}

/**
 * @param {number} row - Row index.
 * @param {number} column - Column index.
 * * */
export function gameTurn(row, column) {
  updateBoard(row, column);
  increaseTurnsCounter();
  const winner = checkWinner();
  if (winner) {
    chooseWinner();
    increaseGameNumber();
    setWinnerSeq(winner);
    return true;
  }
  const hasDraw = checkDraw();
  if (hasDraw) {
    increaseDrawScore();
    increaseGameNumber();
    return true;
  }
  switchPlayer();
  return false;
}

export function resetGame() {
  setWinner(null);
  setWinnerSeq([]);
  state.turns_counter = 0;
  state.current_turn = DEFAULT_CURRENT_TURN;
  state.board = DEFAULT_BOARD();
}

export function getState() {
  return state;
}
