// 'типизация для бедных'
/**  'cross' | 'round'  * */
export const CROSS_PLAYER = "cross";
export const ROUND_PLAYER = "round";
export const DEFAULT_CURRENT_TURN = CROSS_PLAYER;

// don't need to calculate winner during less or equal than this value
export const MIN_NEEDED_TURNS = 4;

/** string | null [][] * */
export const DEFAULT_BOARD = () => [
  ...[
    [null, null, null],
    [null, null, null],
    [null, null, null],
  ],
];

export const state = {
  game_number: 1,
  cross_score: 0,
  round_score: 0,
  draw_score: 0,
  turns_counter: 0,
  current_turn: DEFAULT_CURRENT_TURN,
  winner: null,
  winner_seq: [],
  board: DEFAULT_BOARD(),
};
