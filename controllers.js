import { getState, resetGame, gameTurn } from "./services";

const renderEvent = new Event("render");

function renderTable(board, winner) {
  const gameBoard = document.querySelector(".game-table");
  const tableRows = gameBoard.children;
  board.forEach((row, rowIndex) => {
    const tableCells = tableRows[rowIndex].children;
    row.forEach((cell, cellIndex) => {
      const tableCell = tableCells[cellIndex];
      const cellValue = tableCell.querySelector(".game-cell");
      if (cell) {
        cellValue.classList.add(cell);
      } else {
        cellValue.className = "game-cell";
      }
      if (winner.length) {
        if (
          winner.some((item) =>
            item.every((value, index) => value === [rowIndex, cellIndex][index])
          )
        ) {
          cellValue.classList.add("victory");
        } else {
          cellValue.classList.add("disabled");
        }
      }
    });
  });
}

function renderCurrentTurn(value) {
  const currentTurn = document.querySelector("[data-current-turn]");
  if (currentTurn) {
    currentTurn.className = value;
    currentTurn.textContent = value;
  }
}

function renderWinner(value) {
  const winner = document.querySelector("[data-winner]");
  const winnerText = document.querySelector("[data-winner-text]");
  if (value && winner && winnerText) {
    winner.className = "show";
    winnerText.className = value;
    winnerText.textContent = value;
  } else {
    winner.className = "hide";
  }
}

// updated life cycle event
function update() {
  const application = document.querySelector("#mount-my-new-framework");
  application.dispatchEvent(renderEvent);
}

function render() {
  const state = getState();
  const gameNumber = document.querySelector(".game");
  const crossScore = document.querySelector(".cross-score");
  const roundScore = document.querySelector(".round-score");
  const drawScore = document.querySelector(".draw-score");
  if (gameNumber) {
    gameNumber.textContent = state.game_number;
  }
  if (crossScore) {
    crossScore.textContent = state.cross_score;
  }
  if (roundScore) {
    roundScore.textContent = state.round_score;
  }
  if (drawScore) {
    drawScore.textContent = state.draw_score;
  }
  renderCurrentTurn(state.current_turn);
  renderWinner(state.winner);
  renderTable(state.board, state.winner_seq);
}

function clickCellHandler(event) {
  if (
    event.target.classList.contains("game-cell") &&
    event.target.classList.length === 1
  ) {
    gameTurn(
      Number(event.target.parentElement.dataset.row) - 1,
      Number(event.target.parentElement.dataset.column) - 1
    );
    update();
  }
}

function resetButtonHandler() {
  resetGame();
  update();
}

function subscribe() {
  const gameBoard = document.querySelector(".game-table");
  if (gameBoard) {
    gameBoard.addEventListener("click", clickCellHandler);
  }
  const gameReset = document.querySelector(".game-reset");
  if (gameReset) {
    gameReset.addEventListener("click", resetButtonHandler);
  }
}

export default function init() {
  // beforeCreate life cycle event
  const application = document.querySelector("#mount-my-new-framework");
  if (application) {
    application.addEventListener("render", render);
  }

  subscribe();

  // created life cycle event
  application.dispatchEvent(renderEvent);
}
